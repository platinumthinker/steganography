#!/usr/bin/ipython

from LSB_replace_random.implement import implement
import os
import sys
import Image


image = Image.open(sys.argv[1])
pix = image.load()

implement(pix, image.size[0], image.size[1], float(sys.argv[2]))

image.save(sys.argv[1] + "_new.bmp", "BMP")
