#!/usr/bin/ipython

import shift.shift
import rs.rs
import ws.ws
import ws_c.ws
import LSB_replace_random.implement
import Image
import sys
import os

def one_image_test(filename, percent, metod):
    image = Image.open(filename)
    pixels = image.load()

    height = image.size[0]
    width  = image.size[1]

    LSB_replace_random.implement.implement(pixels, height, width, percent)

    return metod.analize(image)

def image_pack_test(filepath, percent, metod, folder):
    buf = 0
    count = 0
    result = []
    f = open(folder + "/" + str(percent)+".txt", 'w')
    for image in os.listdir(filepath):
        fileimage = str(filepath + "/" + image)
        result.append(one_image_test(fileimage, percent, metod))
        f.write(str(result[count]) + "\n")
        f.flush()
        count += 1
    f.close()
    return result

def create_roc(img, metod, folder):
    inject_list = [0, 2.5, 5, 10]
    for inject in inject_list:
        print "Calculate for inject: " + str(inject)
        image_pack_test(img, inject, metod, folder)
        print "Done!"

#print one_image_test(sys.argv[1], float(sys.argv[2]), ws.ws)

if len(sys.argv) < 1:
    print "ROC diagram create!"
    print "Usage:"
    print "     ./roc_create.py FOLDER metod"
    print "Where metod:"
    print "shift rs ws"

if sys.argv[2] == "shift":
    create_roc(sys.argv[1], shift.shift, sys.argv[2])
elif sys.argv[2] == "rs":
    create_roc(sys.argv[1], rs.rs, sys.argv[2])
elif sys.argv[2] == "ws":
    create_roc(sys.argv[1], ws.ws, sys.argv[2])
elif sys.argv[2] == "ws_c":
    create_roc(sys.argv[1], ws_c.ws, sys.argv[2])
else:
    print "Unknown metod!"
