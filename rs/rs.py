#!/usr/bin/python

import math
import sys
import Image

positivePattern = [0, 1, 0]
negativePattern = [0, 0, 0]

for i in xrange(len(positivePattern)):
    negativePattern[i] = -positivePattern[i];

def flip(value, mode):
    if mode is 1:
        return value ^ 1
    elif mode is 0:
        return value
    elif mode is -1:
        return (((value + 1) ^ 1) - 1)

def discriminator(raster, first, length, size, flipPattern, width, channel):
    result = 0
    i = 0
    while i < length - 1 and i + first + 1 < size:
        x1 = (first + i + 1) % width
        y1 = (first + i + 1) / width
        x2 = (first + i) % width
        y2 = (first + i) / width
        if flipPattern is "null":
            result += math.fabs(raster[x1, y1][channel] -\
                    raster[x2, y2][channel])
        else:
            result += math.fabs(flip(raster[x1, y1][channel], flipPattern[i+1])\
                    - flip(raster[x2, y2][channel], flipPattern[i]) )
        i += 1

    return result

def classifySample(raster, first, length, size, flipPattern, width, channel):
    normal = discriminator(raster, first, length, size, "null", width, channel)
    flipped = discriminator(raster, first, length, size, flipPattern, width,\
            channel)

    if (flipped > normal):
        return "REGULAR"
    elif (flipped < normal):
        return "SINGULAR"
    else:
        return "UNUSABLE"

def analize(img):
    (width, height) = img.size
    raster = img.load()

    groupLength = len(positivePattern)
    size = width * height

    sum = 0.0
    for channel in xrange(len(img.getbands())):
        sum += analize_channel(raster, width, height,\
                channel, groupLength, size)
    return sum / .03

def analize_channel(raster, width, height, channel, groupLength, size):
    reg_pos_orig = 0
    sing_pos_orig = 0
    reg_neg_orig = 0
    sing_neg_orig = 0

    reg_pos_flip = 0
    sing_pos_flip = 0
    reg_neg_flip = 0
    sing_neg_flip = 0

    for groupStart in xrange(0, size, groupLength):
        group = classifySample(raster, groupStart, groupLength, size,\
                positivePattern, width, channel)

        if group is "REGULAR":
            reg_pos_orig += 1
        elif group is "SINGULAR":
            sing_pos_orig += 1

        group = classifySample(raster, groupStart, groupLength, size,\
                negativePattern, width, channel)
        if group is "REGULAR":
            reg_neg_orig += 1
        elif group is "SINGULAR":
            sing_neg_orig += 1

    for groupStart in xrange(0, size, groupLength):
        i = 0
        while i < groupLength and i + groupStart < size:
            x = (i + groupStart) % width
            y = (i + groupStart) / width
            buf = list(raster[x, y])
            buf[channel] ^= 1
            raster[x, y] = tuple(buf)
            i += 1

        group = classifySample(raster, groupStart, groupLength, size,\
                positivePattern, width, channel)
        if group is "REGULAR":
            reg_pos_flip += 1
        elif group is "SINGULAR":
            sing_pos_flip += 1

        group = classifySample(raster, groupStart, groupLength, size,\
                negativePattern, width, channel)
        if group is "REGULAR":
            reg_neg_flip += 1
        elif group is "SINGULAR":
            sing_neg_flip += 1

        i = 0
        while i < groupLength and i + groupStart < size:
            x = (i + groupStart) % width
            y = (i + groupStart) / width
            buf = list(raster[x, y])
            buf[channel] ^= 1
            raster[x, y] = tuple(buf)
            i += 1

    return intersectCurves(reg_pos_orig, sing_pos_orig, reg_neg_orig,\
            sing_neg_orig, reg_pos_flip, sing_pos_flip, reg_neg_flip,\
            sing_neg_flip)

def intersectCurves(reg_pos_orig, sing_pos_orig, reg_neg_orig, sing_neg_orig,\
        reg_pos_flip, sing_pos_flip, reg_neg_flip, sing_neg_flip):
#R_M(p/2) - S_M(p/2)
    d0 = reg_pos_orig - sing_pos_orig
#R_M(1-p/2) - S_M(1-p/2)
    d1 = reg_pos_flip - sing_pos_flip
#R_{-M}(p/2) - S_{-M}(p/2)
    dn0 = reg_neg_orig - sing_neg_orig
#R_{-M}(1-p/2) - S_{-M}(1-p/2)
    dn1 = reg_neg_flip - sing_neg_flip

    a = 2 * (d1 + d0)
    b = dn0 - dn1 - d1 - 3 * d0
    c = d0 - dn0

    x = solve(a, b, c)
    return x / (x - 0.5)

def solve(a, b, c):
    d = (b * b) - (4 * a * c)

    if d < 0 or a == 0:
        return 0

    x1 = (-b + math.sqrt(d)) / (2.0 * a)
    x2 = (-b - math.sqrt(d)) / (2.0 * a)

    return min(x1, x2)
