#!/usr/bin/python

import Image
import random
import math
import os
import sys

def encode(image, data, key):
    random.seed(key)
    img = Image.open(image)
    
    containerSize = math.floor( img.size[0] * img.size[1] / 8 )
    data_size = os.stat(data).st_size
    if containerSize < data_size:
        print "Size data file(", data_size ,") is a more than size picture (",\
            containerSize, ")"
        sys.exit()

    with open(data, "rb") as f:
        pixels = img.load()
        pixel = 0
        size = img.size[1]
        indexes = list(xrange(0, size * img.size[0]))
        random.shuffle(indexes)
        for line in f:
            for byte in line:
                for bit in bin(ord(byte))[2:].rjust(8, '0'):
                    i = indexes[pixel] % size
                    j = int(indexes[pixel] / size)
                    pixel += 1
                    (r, g, b) = pixels[i,j]
                    if (r & 1) is not int(bit): 
                        r ^= 1
                        pixels[i,j] = (r, g, b)

    #img.show()
    basename = image.split('/')[-1].split('.')[0]
    img.save(basename + "_new.bmp", "BMP")

if len(sys.argv) <= 2:
    print "Hello world in steganography!"
    print "Usage:"
    print "     ./lab01.py picture datafile key"
    sys.exit()

encode(sys.argv[1], sys.argv[2], sys.argv[3])
