#!/usr/bin/ipython

import random

def implement(image, height, width, percent):
    size = int(height * width)
    conteiner_size = size * 3
    implement_bit = int(conteiner_size * percent / 100.0)

    indexes = list(range(0, conteiner_size))
    random.shuffle(indexes)

    for pixel in xrange(implement_bit):
        channel = indexes[pixel] / size
        pixel_ch = indexes[pixel] % size
        i = pixel_ch % height
        j = int(pixel_ch / height)
        if random.randrange(2) is 0:
            buf = list(image[i, j])
            buf[channel] ^= 1
            image[i, j] = tuple(buf)
    return image
