#!/usr/bin/ipython
import sys

def xfrange(start, stop, step):
    if start > stop:
        while start > stop:
            yield start
            start -= step
    else:
        while start < stop:
            yield start
            start += step

def read_file(filename):
    with open(filename) as f:
        lines = f.readlines()
        data = []
        for i in lines:
            data.append(float(i))
        return data

def get_th(data, th):
    val = 0
    for i in data:
        if i >= th:
            val += 1
    return val/10.

def create_custom_inject(inpt, out):
    full  = read_file(inpt)
    max_val = max(empty[-1], full[-1])
    min_val = min(empty[0], full[0])
    min_val = min(min_val, 0)
    f = open(out, "w")
    for th in xfrange(min_val, max_val, (max_val - min_val) / 10000.):
        x = get_th(empty, th)
        y = get_th(full, th)
        f.write(str(x) + " " + str(y) + "\n")
    f.close()


if len( sys.argv ) < 2:
    print "Usage"
    print "./graphics.py METOD"
    print "     when METOD = rs, ws, shift"
    sys.exit()

empty = read_file(sys.argv[1] + "/0.txt")
create_custom_inject(sys.argv[1] + "/2.5.txt", sys.argv[1] + "/data1.txt")
create_custom_inject(sys.argv[1] + "/5.txt",   sys.argv[1] + "/data2.txt")
create_custom_inject(sys.argv[1] + "/10.txt",  sys.argv[1] + "/data3.txt")
