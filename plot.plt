#!/usr/bin/gnuplot

metod = "ws_c"
set terminal pngcairo mono enhanced size 800,600
set termoption dashed
set grid
set key top left reverse Right box 1

set title metod
set xlabel "False positive"
set ylabel "True positive"

set output metod . "/roc.png"

set style line 1 lt 1 lw 0.5
set style line 2 lt 2 lw 1
set style line 3 lt 3 lw 1
set style line 4 lt 4 lw 1

plot [0:100] [0:100] x notitle ls 1,\
         metod . '/data1.txt' u 1:2 title '2.5' ls 2 w l,\
         metod . '/data2.txt' u 1:2 title '5'   ls 3 w l,\
         metod . '/data3.txt' u 1:2 title '10'  ls 4 w l
