#!/usr/bin/ipython

import Image
import os
import sys
import numpy


# Filter
# e _ _ _ f
# _ a _ b _
# _ _ _ _ _
# _ c _ d _
# e _ _ _ f
def getFilter():
    return [[[-1, +1]],             #a
            [[+1, +1]],             #b
            [[-1, -1]],             #c
            [[+1, -1]],             #d
            [[-2, -2], [-2, +2]],   #e
            [[+2, +2], [+2, -2]]]   #f

def filterFunc(w, height, width, channel, f):
    def filt(img, i, j):
        res = 0.0
        for wi, fi in zip(w, f):
            s = 0.0
            for [x, y] in fi:
                x1 = i + x
                y1 = j + y
                if x1 >= 0 and x1 < width and y1 >= 0 and y1 < height:
                    s += img[x1, y1][channel]
            res += s * wi
        return res
    return filt

def add(i, j, image, f, channel, height, width):
    res = []
    for fi in f:
        s = 0.0
        for [x, y] in fi:
            x1 = i + x
            y1 = j + y
            if x1 >= 0 and x1 < width and y1 >= 0 and y1 < height:
                s += image[x1, y1][channel]
            else:
                s = 0.0
                break
        res.append(s)
    return res

def calculateFilter(img, height, width, channel):
    #n = 6
    #m = 7
    #matrix = []
    #for i in xrange(n):
        #matrix.append([0.0] * n)
    #X = []
    #for i in xrange(width):
        #X.append([])
    #res = [0.0] * n

    f = getFilter()
    #for i in xrange(width):
        #for j in xrange(height):
            #X[i].append( add(i, j, img, f, channel, height, width) )

    #for x in xrange(width):
        #for y in xrange(height):
            #for i in xrange(n):
                #for j in xrange(n):
                    #matrix[i][j] += X[x][y][i] * X[x][y][j]

    #for x in xrange(width):
        #for y in xrange(height):
            #for i in xrange(n):
                #res[i] += img[x,y][channel] * X[x][y][i]

    #weights = list(numpy.linalg.solve(matrix, res))
    weights = [1.0/6, 1.0/6, 1.0/6, 1.0/6, 1.0/12, 1.0/12]
    return filterFunc(weights, height, width, channel, f)

def analize_one_img(img, channel, (width, height)):
    filt = calculateFilter(img, height, width, channel)
    s = 0.0
    for x in xrange(width):
        for y in xrange(height):
            pixel = img[x,y][channel]
            s += (pixel - filt(img, x, y)) * (pixel - (pixel ^ 1))
    val = 2. * s / (width * height)
    return val

def analize(img):
    image = img.load()
    s = 0.0
    for i in xrange(len(img.getbands())):
        s += analize_one_img(image, i, img.size)
    return s/0.03
