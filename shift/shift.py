#!/usr/bin/ipython

import Image
import math
import os
import sys
import random
import bz2
import lzma
import gzip
import array
from LSB_replace_random.implement import implement

def xfrange(start, stop, step):
    if start > stop:
        while start > stop:
            yield start
            start -= step
    else:
        while start < stop:
            yield start
            start += step

def shift(image, height, width):
    for i in xrange(1, height):
        for j in xrange(1, width):
            (r, g, b) = image[i, j]
            image[i,j] = (r >> 1, g >> 1, b >> 1)
    return image

def inc(image, height, width):
    for i in xrange(1, height):
        for j in xrange(1, width):
            (r, g, b) = image[i, j]
            if r is not 255: r += 1
            else: r -= 1
            if g is not 255: g += 1
            else: g -= 1
            if b is not 255: b += 1
            else: b -= 1
            image[i,j] = (r, g, b)
    return image

def dec(image, height, width):
    for i in xrange(1, height):
        for j in xrange(1, width):
            (r, g, b) = image[i, j]
            if r is not 0: r -= 1
            else: r += 1
            if g is not 0: g -= 1
            else: g += 1
            if b is not 0: b -= 1
            else: b += 1
            image[i,j] = (r, g, b)
    return image

def getCompessSize(image, height, width):
    data = array.array('B')
    for pixel in xrange(1, height * width):
        i = pixel % height
        j = int(pixel / height)
        (r, g, b) = image[i, j]
        data.append(r)
        data.append(g)
        data.append(b)
    size = len(lzma.compress(data.tostring()))
    return size

def get_imp_size(image, height, width, percent):
    image = implement(image, height, width, percent)
    return getCompessSize(image, height, width)

def analize(image):
    pixels = image.load()

    height = image.size[0]
    width  = image.size[1]

##Inc
    im1 = image.copy()
    inc_pix = im1.load()
    inc(inc_pix, height, width)
    shift(inc_pix, height, width)
    inc_size = getCompessSize(inc_pix, height, width)

##Dec
    im2 = image.copy()
    dec_pix = im2.load()
    dec(dec_pix, height, width)
    shift(dec_pix, height, width)
    dec_size = getCompessSize(dec_pix, height, width)

    size = (inc_size + dec_size) / 2.0
    shift(pixels, height, width)

    percent = 0.0
    old_value = size
    res_percent = 0.0
    count_experiment = 3
    for step in xrange(3):
        pr = 10 ** (1 - step)
        if step is 2 and percent > 5:
            break
        for percent in xfrange(percent, 100, pr):
            size_im = 0.0
            for i in range(count_experiment):
                im = image.copy()
                size_im += get_imp_size(im.load(), height, width, percent)
            size_im /= count_experiment

            if old_value > abs(size - size_im):
                old_value = min(old_value, abs(size - size_im))
                res_percent = percent
            if size_im > size:
                if percent - pr >= 0:
                    percent -= pr
                    percent += pr / 10.
                break
    return percent
