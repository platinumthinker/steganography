#!/usr/bin/python

import Image
import math
import os
import sys

def decode(image, data, size):
    img = Image.open(image)
    containerSize = int( img.size[0] * img.size[1] / 8 )
    if size is 0:
        size = containerSize
    if containerSize < size:
        print "Size data (", size ,") is a more than size picture (",\
            containerSize, ")"
        sys.exit()
    
    with open(data, "wb") as f:
        pixels = img.load()
        size1 = img.size[1]
        for pixel in xrange(0, size * 8, 8):
            byte = 0
            for bit in xrange(8):
                i = (pixel + bit) % size1
                j = int((pixel + bit) / size1)
                (r, g, b) = pixels[i,j]
                byte |= ((r & 1) << (7 - bit))
            f.write(chr(byte))

if len(sys.argv) <= 2:
    print "Hello world in steganography!"
    print "Usage:"
    print "     ./decode1.py picture datafile size (byte)"
    sys.exit()

if len(sys.argv) is 3:
    decode(sys.argv[1], sys.argv[2], 0)
else:
    decode(sys.argv[1], sys.argv[2], int(sys.argv[3]))
